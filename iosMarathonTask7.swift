enum Action {
	case startEngine
	case stopEngine 
	case openWindows
	case closeWindows
	case loadTrunk 
	case unloadTrunk 
}

struct Car {
	let mark: String
	let year: Int 
	var engineStarted: Bool = false
	var windowsOpened: Bool = false
	var maxTrunkVolume: Int = 0
	var currentTrunkVolume: Int = 0
	mutating func doAction(action: Action) {
		switch action {
			case .startEngine: engineStarted = true
			case .stopEngine: engineStarted = false
			case .openWindows: windowsOpened = true
			case .closeWindows: windowsOpened = false
			case .loadTrunk: if (currentTrunkVolume < maxTrunkVolume) {currentTrunkVolume += 1}
			case .unloadTrunk: if (currentTrunkVolume > 0) {currentTrunkVolume -= 1}
		}
	}
}

struct Truck {
	let mark: String
	let year: Int 
	var engineStarted: Bool = false
	var windowsOpened: Bool = false
	var maxTrunkVolume: Int = 0
	var currentTrunkVolume: Int = 0
	mutating func doAction(action: Action) {
		switch action {
			case .startEngine: engineStarted = true
			case .stopEngine: engineStarted = false
			case .openWindows: windowsOpened = true
			case .closeWindows: windowsOpened = false
			case .loadTrunk: if (currentTrunkVolume < maxTrunkVolume) {currentTrunkVolume += 1}
			case .unloadTrunk: if (currentTrunkVolume > 0) {currentTrunkVolume -= 1}
		}
	}
}


var firstCar = Car(mark: "Vaz", year: 1987, maxTrunkVolume: 35)
var secondCar = Car(mark: "Kia", year: 2009, windowsOpened: true, maxTrunkVolume: 60)
var thirdCar = Car(mark: "BMW", year: 2017, engineStarted: true, maxTrunkVolume: 25, currentTrunkVolume: 1)
firstCar.doAction(action: Action.loadTrunk)
firstCar.doAction(action: Action.loadTrunk)
firstCar.doAction(action: Action.loadTrunk)
firstCar.doAction(action: Action.openWindows)
secondCar.doAction(action: Action.startEngine)
thirdCar.doAction(action: Action.unloadTrunk)
thirdCar.doAction(action: Action.unloadTrunk)

print(firstCar)
print(secondCar)
print(thirdCar)

var dict: [String : Any] = [:]
dict[firstCar.mark] = firstCar
dict[secondCar.mark] = secondCar
dict[thirdCar.mark] = thirdCar

/* 
strong -- обычные ссылки, автоматически используемые при декларации свойств
При использовании strong ссылки объект не удалится ARC
weak -- указатели на объект, могут быть очищены и превращены в nil
unowned -- аналог weak ссылки, но в отличии от них они не опциональны
так же unowned ссылка не обнуляется, когда объект очищается
 */

class Car {
	var driver: Man?
	deinit { //выведем в консоль сообщение о том что объект удален print("машина удалена из памяти")
		print("машина удалена из памяти")
	}
}

class Man {
	weak var myCar: Car?
	deinit{ //выведем в консоль сообщение о том что объект удален
		print("мужчина удален из памяти")
	}
}

//Обхявим переменные как опциональные, что бы иметь возможность присвоить им nil
var car: Car? = Car()
var man: Man? = Man()
//машина теперь имеет ссылку на мужчину
car?.driver = man
//а мужчина на машину
man?.myCar = car
//присвоим nil переменным, удалим эти ссылки
car = nil
man = nil
//мы больше не можем никак обратится к нашим объектам, но они продолжают существовать в памяти

/* 
	Это происходит потому, что мы декларируем свойства с strong ссылкой, обнуляя ссылку на car и man 
	мы не удаляем объекты, для этого явно объявим driver и myCar как свойства с weak ссылками
*/

class Man {
	var pasport: Passport? // По заданию weak или unowned сюда запрещено ставить!
	deinit { // выведем в консоль сообщение о том, что объект удален 
		print("мужчина удален из памяти")
	}
}

class Passport {
	unowned let man: Man
	init(man: Man) {
		self.man = man
	}
	deinit { // выведем в консоль сообщение о том, что объект удален
		print("паспорт удален из памяти")
	}
}
var man: Man? = Man()
var passport: Passport? = Passport(man: man!) 
man?.pasport = passport 
passport = nil // объект еще не удален, его удерживает мужчина 
man = nil // теперь удалены оба объекта






